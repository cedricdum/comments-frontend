/* eslint-disable import/no-unresolved */
import AuthenticatedLayout from 'src/components/layout/AuthenticatedLayout.vue';

const routes = [
  {
    path: '/',
    redirect: '/channels',
    component: AuthenticatedLayout,
    children: [
      {
        path: 'channels',
        name: 'Channels',
        component: () => import('src/views/channels/Channels.vue'),
        meta: {
          access: {
            public: false,
            needAdminRole: false,
          },
        },
      },
      {
        path: 'admin',
        name: 'Admin',
        component: () => import('src/views/admin/Admin.vue'),
        meta: {
          access: {
            public: false,
            needAdminRole: true,
          },
        },
      },
    ],
  },
];

export default routes;
