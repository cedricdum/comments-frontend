import Vue from 'vue';
import VueRouter from 'vue-router';
import store from 'src/store';

import publicRoutes from './public.routes';
import privateRoutes from './private.routes';

Vue.use(VueRouter);

const routes = [
  ...publicRoutes,
  ...privateRoutes,
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeResolve((to, from, next) => {
  const isPublic = to.meta.access.public;
  const { token } = store.state.authentication;

  if (isPublic && !token) {
    next();
    return false;
  }

  if (isPublic && token) {
    store.dispatch('snackbar/error', 'Vous devez être deconnecté pour accéder à cette page');
    next('/channels');
    return false;
  }

  if (!isPublic && !token) {
    store.dispatch('snackbar/error', 'Vous devez être connecté pour accéder à cette page');
    next('/login');
    return false;
  }

  next();
  return true;
});

export default router;
