import UnauthenticatedLayout from 'src/components/layout/UnauthenticatedLayout.vue';

const routes = [
  {
    path: '/',
    redirect: '/login',
    component: UnauthenticatedLayout,
    children: [
      {
        path: 'login',
        name: 'Login',
        component: () => import('src/views/authentication/Login.vue'),
        meta: {
          access: {
            public: true,
            roles: null,
          },
        },
      },
    ],
  },
];

export default routes;
