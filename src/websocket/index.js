import { io } from 'socket.io-client';
import store from 'src/store';

const url = process.env.VUE_APP_WSS_URL || 'ws://localhost:8889/';
const socket = io(url, { transports: ['websocket', 'xhr-polling', 'htmlfile', 'jsonp-polling'] });

socket.on('connect', () => {
  console.log('Socket connected:', socket.id);
});

socket.on('connect_error', (error) => {
  console.log('Socker error:', error);
});

socket.on('disconnect', () => {
  console.log('Socket disconnected');
});

// Need to be in Channel.vue and vuex
socket.on('updateChannel', (channel) => {
  console.log('data', channel);
  store.dispatch('channels/updateOneFromSocket', channel);
  store.dispatch('snackbar/success', 'commentaire ajouté');
});

export default socket;
