import {
  ValidationProvider, ValidationObserver, extend, localize,
} from 'vee-validate';

import * as rules from 'vee-validate/dist/rules';
import fr from 'vee-validate/dist/locale/fr.json';

Object.keys(rules).forEach((rule) => {
  extend(rule, rules[rule]);
});

// From: https://stackoverflow.com/questions/44683790/vee-validate-require-either-of-two-fields
extend('required_if_not', {
  ...rules.required_if,
  validate: (value, args) => {
    const targetValue = args.target;
    return Boolean(targetValue || value);
  },
  message: 'Remplissez le champ commande et/ou le champ coordonnées',
});

// from: https://stackoverflow.com/questions/3518504/regular-expression-for-matching-latitude-longitude-coordinates
extend('coordinates', {
  validate: (value) => {
    const coordinates = new RegExp(
      '^[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?),\\s*[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)$',
    );

    if (coordinates.test(value)) return true;
    return 'Longitude [-180, 180] et latitude [-90, 90] séparés par une virgule. Exemple: -73.856, 40.848';
  },
});

localize({ fr });
localize('fr');

export default {
  install(Vue) {
    Vue.component('validation-provider', ValidationProvider);
    Vue.component('validation-observer', ValidationObserver);
  },
};
