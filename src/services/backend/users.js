/* eslint-disable import/prefer-default-export */

import { getAuthenticatedRequest as api } from './requestBuilder';

const baseURL = '/users';

export async function createOne(payload) {
  const url = `${baseURL}`;
  const { data } = await api().post(url, payload);
  return data;
}

export async function getAll() {
  const url = `${baseURL}`;
  const { data } = await api().get(url);
  return data;
}

export async function updateOne(id, payload) {
  const url = `${baseURL}/${id}`;
  const { data } = await api().put(url, payload);
  return data;
}

export async function deleteOne(id) {
  const url = `${baseURL}/${id}`;
  const { data } = await api().delete(url);
  return data;
}
