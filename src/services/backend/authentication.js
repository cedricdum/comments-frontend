/* eslint-disable import/prefer-default-export */

import { getUnauthenticatedRequest } from './requestBuilder';

export async function login(payload) {
  const url = '/login';
  const { data } = await getUnauthenticatedRequest().post(url, payload);
  return data;
}
