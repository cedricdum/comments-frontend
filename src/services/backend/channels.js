/* eslint-disable import/prefer-default-export */

import { getAuthenticatedRequest as api } from './requestBuilder';

const baseURL = '/channels';

export async function getAll() {
  const url = `${baseURL}`;
  const { data } = await api().get(url);
  return data;
}

export async function getOne(payload) {
  const url = `${baseURL}/getOne`;
  const { data } = await api().get(url, payload);
  return data;
}

export async function updateOne(payload) {
  const url = `${baseURL}/updateOne`;
  const { data } = await api().put(url, payload);
  return data;
}

export async function deleteOne(id) {
  const url = `${baseURL}/${id}`;
  const { data } = await api().delete(url);
  return data;
}
