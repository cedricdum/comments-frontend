import axios from 'axios';
import store from 'src/store';

const baseURL = process.env.VUE_APP_BACKEND_URL || 'http://localhost:8888';
const timeout = 60000;
const defaultHeaders = {
  Accept: '*/*',
  'Access-Control-Allow-Origin': '*',
  'content-type': 'application/json',
  'Access-Control-Allow-Headers':
    'Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization',
  'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT,DELETE',
  'Access-Control-Allow-Credentials': 'true',
};

export function getAuthenticatedRequest() {
  const { token } = store.state.authentication;
  const headers = { ...defaultHeaders, token };
  const httpClient = axios.create({ baseURL, timeout, headers });

  return httpClient;
}

export function getUnauthenticatedRequest() {
  const headers = { ...defaultHeaders };
  const httpClient = axios.create({ baseURL, timeout, headers });
  return httpClient;
}
