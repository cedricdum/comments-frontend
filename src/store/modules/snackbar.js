export const SET_COLOR = 'SET_COLOR';
export const SET_MESSAGE = 'SET_MESSAGE';
export const SET_OPEN = 'SET_OPEN';

export default {
  namespaced: true,
  state: {
    open: false,
    color: 'success',
    message: '',
  },
  getters: {
    open(state) {
      return state.open;
    },
  },
  actions: {
    success({ commit }, message) {
      commit(SET_MESSAGE, { color: 'success', message });
    },
    error({ commit }, message) {
      commit(SET_MESSAGE, { color: 'error', message });
    },
    warning({ commit }, message) {
      commit(SET_MESSAGE, { color: 'warning', message });
    },
    info({ commit }, message) {
      commit(SET_MESSAGE, { color: 'info', message });
    },
    defaultError({ commit }) {
      commit(SET_MESSAGE, {
        color: 'error',
        message: 'Une erreur inconnue est survenue',
      });
    },
  },
  mutations: {
    [SET_COLOR](state, color) {
      state.color = color;
    },
    [SET_MESSAGE](state, { color, message }) {
      if (color) state.color = color;
      if (message) state.message = message;
      state.open = true;
    },
    [SET_OPEN](state, open) {
      state.open = open;
    },
  },
};
