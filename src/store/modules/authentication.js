/* eslint-disable import/named */
import { authenticationApi } from 'src/services/backend';

export const SET_TOKEN = 'SET_TOKEN';
export const SET_USERID = 'SET_USERID';
export const SET_ROLE = 'SET_ROLE';

export default {
  namespaced: true,
  state: {
    token: '',
    userId: '',
    role: '',
  },
  actions: {
    async login({ commit }, { username, password }) {
      const { token, userId, role } = await authenticationApi.login({ username, password });
      commit(SET_TOKEN, token);
      commit(SET_USERID, userId);
      commit(SET_ROLE, role);
      return token;
    },
    reset({ commit }) {
      commit(SET_TOKEN, '');
      commit(SET_USERID, '');
      commit(SET_ROLE, '');
    },
  },
  mutations: {
    [SET_TOKEN](state, token) {
      state.token = token;
    },
    [SET_USERID](state, userId) {
      state.userId = userId;
    },
    [SET_ROLE](state, role) {
      state.role = role;
    },
  },
};
