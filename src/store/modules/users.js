/* eslint-disable import/named, import/no-cycle */
import { usersApi } from 'src/services/backend';

export const SET_ALL = 'SET_ALL';
export const ADD_ONE = 'ADD_ONE';
export const UPDATE_ONE = 'UPDATE_ONE';
export const DELETE_ONE = 'DELETE_ONE';

export default {
  namespaced: true,
  state: {
    all: [],
  },
  actions: {
    async createOne({ commit }, payload) {
      const user = await usersApi.createOne(payload);
      commit(ADD_ONE, user);
      return user;
    },
    async fetchAll({ commit }) {
      const users = await usersApi.getAll();
      commit(SET_ALL, users);
      return users;
    },
    async updateOne({ commit, state }, user) {
      const userUpdated = await usersApi.updateOne(user._id, user);
      const index = state.all.findIndex(({ _id }) => _id === userUpdated._id);
      if (index < 0) {
        commit(ADD_ONE, userUpdated);
      } else {
        commit(UPDATE_ONE, { user: userUpdated, index });
      }
      return userUpdated;
    },
    async deleteOne({ commit, state }, id) {
      await usersApi.deleteOne(id);
      const index = state.all.findIndex(({ _id }) => _id === id);
      commit(DELETE_ONE, index);
    },
    reset({ commit }) {
      commit(SET_ALL, []);
    },
  },
  mutations: {
    [SET_ALL](state, users) {
      state.all = [...users];
    },
    [ADD_ONE](state, user) {
      state.all.push(user);
    },
    [UPDATE_ONE](state, { index, user }) {
      state.all.splice(index, 1, user);
    },
    [DELETE_ONE](state, index) {
      state.all.splice(index, 1);
    },
  },
};
