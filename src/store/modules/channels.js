/* eslint-disable import/named, import/no-cycle */
import { channelsApi } from 'src/services/backend';

export const SET_ALL = 'SET_ALL';
export const SET_CURRENT_INDEX = 'SET_CURRENT_INDEX';
export const ADD_ONE = 'ADD_ONE';
export const UPDATE_ONE = 'UPDATE_ONE';
export const DELETE_ONE = 'DELETE_ONE';

export default {
  namespaced: true,
  state: {
    all: [],
    indexCurrent: 0,
  },
  getters: {
    current(state) {
      return state.all[state.indexCurrent];
    },
  },
  actions: {
    async fetchAll({ commit, getters }) {
      const oldCurrentChannel = getters.current;
      const channels = await channelsApi.getAll();
      commit(SET_ALL, channels);
      const newCurrentChannel = getters.current;
      if (!oldCurrentChannel || oldCurrentChannel._id !== newCurrentChannel._id) {
        commit(SET_CURRENT_INDEX, 0);
      }
    },
    async fetchOne({ commit, state }, id) {
      const channel = await channelsApi.getOne(id);
      const index = state.all.findIndex(({ _id }) => _id === channel._id);
      if (index > -1) commit(UPDATE_ONE, { channel, index });
      else commit(ADD_ONE, channel);
      return channel;
    },
    async updateOne({ commit, state }, payload) {
      const channel = await channelsApi.updateOne(payload);
      const index = state.all.findIndex(({ _id }) => _id === channel._id);
      if (index < 0) {
        commit(ADD_ONE, channel);
      } else {
        commit(UPDATE_ONE, { channel, index });
      }
      return channel;
    },
    async updateOneFromSocket({ commit, state }, channel) {
      const index = state.all.findIndex(({ _id }) => _id === channel._id);
      if (index < 0) {
        commit(ADD_ONE, channel);
      } else {
        commit(UPDATE_ONE, { channel, index });
      }
    },
    async deleteOne({ commit, state }, id) {
      await channelsApi.deleteOne(id);
      const index = state.all.findIndex(({ _id }) => _id === id);
      commit(DELETE_ONE, index);
    },
    setCurrent({ commit, state }, channel) {
      const index = state.all.findIndex(({ _id }) => _id === channel._id);
      if (index > -1) {
        commit(SET_CURRENT_INDEX, index);
      } else {
        commit(ADD_ONE, channel);
        commit(SET_CURRENT_INDEX, state.all.length - 1);
      }
    },
    reset({ commit }) {
      commit(SET_ALL, []);
      commit(SET_CURRENT_INDEX, 0);
    },
  },
  mutations: {
    [SET_ALL](state, channels) {
      state.all = [...channels];
    },
    [SET_CURRENT_INDEX](state, index) {
      state.indexCurrent = index;
    },
    [ADD_ONE](state, channel) {
      state.all.push(channel);
    },
    [UPDATE_ONE](state, { index, channel }) {
      state.all.splice(index, 1, channel);
    },
    [DELETE_ONE](state, index) {
      state.all.splice(index, 1);
    },
  },
};
