import Vue from 'vue';
import Vuex from 'vuex';

import authentication from './modules/authentication';
import channels from './modules/channels';
import users from './modules/users';
import snackbar from './modules/snackbar';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    authentication,
    channels,
    users,
    snackbar,
  },
});
